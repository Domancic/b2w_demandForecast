Forecasting model for B2W price and demand relationship. Includes linear regression and random forest models
which use prices to predict product quantities sold. 