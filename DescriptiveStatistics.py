import pandas
import sqlite3
from datetime import date, datetime

filepath = "C:\Users\Mirna\Desktop\B2W\\"
sales = pandas.read_csv(filepath + "sales.csv")
comp_prices = pandas.read_csv(filepath + "comp_prices.csv")

#sales - 351,092 obs
#comp - 50,114 obs
print("Sales description")
print (sales.describe())

print("\n Competitors description")
print (comp_prices.describe())

print("Sales product id")
print(sales['PROD_ID'].value_counts())

print("Competitors product ids")
print(comp_prices['PROD_ID'].value_counts())

print("Competitors")
print(comp_prices['COMPETITOR'].value_counts())

#modify the date in competitor table
comp_prices['DATE_EXTRACTION'] = comp_prices['DATE_EXTRACTION'].apply(lambda x: x[:10])

print("Modified dates")
print(comp_prices.iloc[1:10,:])


#Create a database and populate with tables Sales and Comp_Prices
connection = sqlite3.connect(filepath + "b2wDatabase.db")
c = connection.cursor()
c.execute('CREATE TABLE Sales (PROD_ID text, DATE_ORDER date, QTY_ORDER real, PRICE real)')
c.execute('CREATE TABLE Comp_Prices (PROD_ID text, DATE_EXTRACTION date, COMPETITOR text, COMPETITOR_PRICE real, PAY_TYPE text)')

#Populate the tables using the data from pandas dataframe
sales.to_sql(name = 'Sales', con = connection, if_exists='replace', index=False)
comp_prices.to_sql(name = 'Comp_Prices', con = connection, if_exists='replace', index=False)

#Check whether the tables are successfully created
for row in c.execute('SELECT * FROM Sales \
	ORDER BY price LIMIT 5'):
        print row

for row in c.execute('SELECT * FROM Comp_Prices \
	ORDER BY competitor_price LIMIT 5'):
    print row

#Additional descriptives using sql 
print "Average prices"
for row in c.execute('SELECT PROD_ID, avg(PRICE) as AVERAGE_PRICE \
	FROM Sales \
	GROUP BY PROD_ID \
	ORDER BY AVERAGE_PRICE'):
	print row

print "Average competitor prices"
for row in c.execute('SELECT PROD_ID, avg(COMPETITOR_PRICE) as AVERAGE_PRICE \
	FROM Comp_Prices \
	GROUP BY PROD_ID \
	ORDER BY AVERAGE_PRICE'):
	print row

print "Quantity sold per product"
for row in c.execute('SELECT PROD_ID, sum(QTY_ORDER) as SOLD \
	FROM Sales \
	GROUP BY PROD_ID \
	ORDER BY SOLD desc'):
	print row

print "Revenue earned by product"
for row in c.execute('SELECT PROD_ID, sum(PRICE) as TOTAL_REVENUE \
	FROM Sales \
	GROUP BY PROD_ID \
	ORDER BY TOTAL_REVENUE desc'):
	print row

#P7 and P2 are our best selling products
#followed by P8, P5, P9, P4, P6, P1, P3

print "Quantity sold per product competitors"
for row in c.execute('SELECT PROD_ID, count(*) as SOLD \
	FROM Comp_Prices \
	GROUP BY PROD_ID \
	ORDER BY SOLD desc'):
	print row

#Create tables with average daily prices	
c.execute('CREATE TABLE AverageDailySales as \
	SELECT *, sum(PRICE) as TOTAL_REVENUE, sum(QTY_ORDER) as NUMBER_SOLD \
	FROM Sales \
	GROUP BY DATE_ORDER,PROD_ID \
	ORDER BY DATE_ORDER, PROD_ID')

c.execute('CREATE TABLE DailyCompetitors as \
	SELECT *, avg(COMPETITOR_PRICE) as AVERAGE_PRICE \
	FROM Comp_Prices \
	GROUP BY DATE_EXTRACTION, PROD_ID, COMPETITOR \
	ORDER BY DATE_EXTRACTION, PROD_ID, COMPETITOR')


c.execute('CREATE TABLE DailySales as \
	SELECT PROD_ID, DATE_ORDER, QTY_ORDER, TOTAL_REVENUE, NUMBER_SOLD, TOTAL_REVENUE/(NUMBER_SOLD/1.47837674) as AVERAGE_PRICE \
	FROM AverageDailySales')

#AverageDailySales is no longer needed, DailySales contains all the important information
c.execute('DROP TABLE AverageDailySales')

#Check success
#for row in c.execute('SELECT * FROM DailySales LIMIT 10'):
#        print row

#for row in c.execute('SELECT * FROM DailyCompetitors LIMIT 10'):
#        print row

#Create a table with transposed columns for competitors
c.execute('CREATE TABLE TransposedCompetitors as SELECT PROD_ID, DATE_EXTRACTION as DATE,\
       MAX(CASE WHEN COMPETITOR = "C1" THEN AVERAGE_PRICE END) C1,\
       MAX(CASE WHEN COMPETITOR = "C2" THEN AVERAGE_PRICE END) C2,\
       MAX(CASE WHEN COMPETITOR = "C3" THEN AVERAGE_PRICE END) C3,\
       MAX(CASE WHEN COMPETITOR = "C4" THEN AVERAGE_PRICE END) C4,\
       MAX(CASE WHEN COMPETITOR = "C5" THEN AVERAGE_PRICE END) C5,\
       MAX(CASE WHEN COMPETITOR = "C6" THEN AVERAGE_PRICE END) C6\
  FROM DailyCompetitors\
 GROUP BY DATE, PROD_ID')


#Merge data from two tables
c.execute('CREATE TABLE MergedData as\
	SELECT s.DATE_ORDER as DATE, s.PROD_ID, s.TOTAL_REVENUE, s.NUMBER_SOLD, s.AVERAGE_PRICE, C1, C2, C3, C4, C5, C6\
	FROM DailySales as s LEFT JOIN TransposedCompetitors as c \
	on s.DATE_ORDER = c.DATE and s.PROD_ID = c.PROD_ID')

#Check output
#for row in c.execute('SELECT * FROM MergedData LIMIT 10'):
#        print row

#Calculate lag using the self join
c.execute("CREATE TABLE MergedData2 as\
	SELECT s2.*, s2.AVERAGE_PRICE-s1.AVERAGE_PRICE as DELTA_AVERAGE, \
	s2.C1-s1.C1 as DELTA_C1, s2.C2-s1.C2 as DELTA_C2,\
	s2.C3-s1.C3 as DELTA_C3, s2.C4-s1.C4 as DELTA_C4,\
	s2.C5-s1.C5 as DELTA_C5, s2.C6-s1.C6 as DELTA_C6\
	FROM MergedData as s2 LEFT JOIN MergedData as s1 \
	on s1.PROD_ID = s2.PROD_ID and date(s1.DATE) = date(datetime(s2.DATE, '-1 day'))")

# Read sqlite query results into a pandas DataFrame
mergedData = pandas.read_sql_query("SELECT * from MergedData2", connection)

# verify that result of SQL query is stored in the dataframe
print(mergedData.head())

#write the file to csv
mergedData.to_csv(filepath +"mergedData.csv")

#close the SQL connection
connection.close()