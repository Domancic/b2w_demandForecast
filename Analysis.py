import pandas
import math
import numpy
from pprint import pprint
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import RandomForestRegressor


filename = "C:\Users\Mirna\Desktop\B2W\\"
data = pandas.read_csv(filename + "mergedData.csv")


#Impute missing values for prices using the mean of daily averages
for i in range(0,data.shape[0]):
	data.iloc[i,5:12] = data.iloc[i,5:12].fillna(data.iloc[i,5:12].mean())

#Impute missing values for deltas assuming that no change took place
for i in range(0,data.shape[0]):
	data.iloc[i,12:19] = data.iloc[i,12:19].fillna(0)

#Exclude the october 14th observations - unusual values for competitors
data = data.iloc[0:2153,:]	

#Create separate datasets for each of the products
p1 = data[data['PROD_ID'] == "P1"]
p2 = data[data['PROD_ID'] == "P2"]
p3 = data[data['PROD_ID'] == "P3"]
p4 = data[data['PROD_ID'] == "P4"]
p5 = data[data['PROD_ID'] == "P5"]
p6 = data[data['PROD_ID'] == "P6"]
p7 = data[data['PROD_ID'] == "P7"]
p8 = data[data['PROD_ID'] == "P8"]
p9 = data[data['PROD_ID'] == "P9"]

#Recode the prod_id column
data["PROD_ID"] = data["PROD_ID"].apply(lambda x: int(x[1]))

#Standardize all the variables, take logs of the ones that have skewed distributions
data.iloc[:,3:19] = data.iloc[:,3:19].apply(lambda x: (x-x.mean()/x.std()))
data['AVERAGE_PRICE'] = numpy.log(data['AVERAGE_PRICE'])
data['C1'] = numpy.log(data['C1'])
data['C2'] = numpy.log(data['C2'])
data['C3'] = numpy.log(data['C3'])
data['C4'] = numpy.log(data['C4'])
data['C5'] = numpy.log(data['C5'])
data['C6'] = numpy.log(data['C6'])
data['AVERAGE_PRICE'] = numpy.log(data['AVERAGE_PRICE'])


#Create the importances data frame
importances = pandas.DataFrame({
        'AVERAGE_PRICE':[], 'DELTA_AVERAGE':[], 'C1':[], 'C2':[], 'C3':[], 'C4':[], 'C5':[], 'C6':[]
    })



#Calculate the correlations
data.corr(method = 'pearson').to_csv(filename + "corrs.csv")
#average price and c1..c6

#Run linear regression with transformed variables
prods = [data,p1,p2,p3,p4,p5,p6,p7,p8,p9]
i = 0
for product in prods:
	#print product.corr(method = 'pearson')
	product.corr(method = 'pearson').to_csv(filename + "corrs" + str(i) +".csv")

	#Remove total revenue - could cause collinearity
	Y = numpy.log(product['NUMBER_SOLD'])
	del product['TOTAL_REVENUE']
	
	#Create the dataset consisting of predictors - change to test different predictor combinations
	X = numpy.array(product[['AVERAGE_PRICE', 'C3', 'C6', 'DELTA_AVERAGE']])

	# Create training and testing subsets and run linear regression to predict prices
	X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
	alg = LinearRegression()
	alg.fit(X_train, y_train)

	print('Variance score for: %.2f' % alg.score(X_test, y_test))

	#Run Random Forest to predict prices
	alg1 = RandomForestRegressor(random_state=1, n_estimators=100, oob_score = True)
	alg1.fit(X, Y)
	
	print('Random forest for {0} : {1:.2f}' .format(i, alg1.oob_score_))

	#Create a table of variable importances
	#importances.loc[i] = alg1.feature_importances_
	i = i+1

#Export the variable importances
#importances.to_csv(filename + "importancesLog2.csv")